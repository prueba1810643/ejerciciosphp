<html> 
	<head> 
		<title>Ejercicios Expresiones Regulares</title>
		<title>Argueta Bravo Angel Jacob </title> 
        <style>
    </style>
	</head> 
	<body> 
		<?php 
			echo "Hecho por Argueta Bravo Angel Jacob <br/>";

        	function comprobarExpresionRegular($expresion_regular, $cadena) {
			    if (preg_match($expresion_regular, $cadena)) {
			        echo "La cadena '$cadena' cumple con la expresión regular 😌.\n";
			        echo "<br/>";
			    } else {
			        echo "La cadena '$cadena' no cumple con la expresión regular ☹️.\n";
			        echo "<br/>";
			    }
			}

			// Comprobar que el  Email sea valido 
			$email_regex = '/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/';
			$email = 'angeljacob938@gmail.com';
			comprobarExpresionRegular($email_regex, $email);
			$email = 'jacob938@gmail';
			comprobarExpresionRegular($email_regex, $email);

			// Comprobar que el CURP sea valido 
			$curp_regex = '/^[A-Z]{1}[AEIOU]{1}[A-Z]{1}[A-Z]{1}[0-9]{6}[HM]{1}[A-Z]{5}[A-Z0-9]{1}[0-9]{1}$/';

			$curp = 'PEGJ850315HJCRRN07';
			comprobarExpresionRegular($curp_regex, $curp);
			$curp = 'auba011003hdf7';
			comprobarExpresionRegular($curp_regex, $curp);

			// Comprobar que la longitud de la palabra sea a 50
			$palabra_regex = '/^[a-zA-Z]{51,}$/';
			$palabra = 'abcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxyabcdefghijklmnopqrstuvwxy';
			comprobarExpresionRegular($palabra_regex, $palabra);
			$palabra = 'abcdefghijklmnopqrstuvwxya';
			comprobarExpresionRegular($palabra_regex, $palabra);

			// Comprobar que el número sea decimal
			$decimal_regex = '/^\d+(\.\d+)?$/';
			$decimal = '123.45';
			comprobarExpresionRegular($decimal_regex, $decimal);
			$decimal = '00';
			comprobarExpresionRegular($decimal_regex, $decimal);

			// Función para Escapar Símbolos Especiales
			function escaparSimbolosEspeciales($texto) {
			    return preg_quote($texto, '/');
			}
			
			$texto_original = 'Este es un texto con [!@#$%^&*()_+-=[]{}|;":",./<>?] especiales.';
			$texto_escaped = escaparSimbolosEspeciales($texto_original);
			echo "Texto original: $texto_original\n";
			echo "<br/>";
			echo "Texto escapado: $texto_escaped\n";
			echo "<br/>";
			// Ejemplo de preg_replace
			$texto_modificado = preg_replace('/[!@#$%^&*()_+\-=\[\]{};:\'",.<>?]/', '', $texto_original);
			echo "Texto modificado: $texto_modificado\n";

			?>
	</body>
</html>
