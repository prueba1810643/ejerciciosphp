<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <title>Argueta Bravo Angel Jacob</title>
    <!-- Enlaces a archivos de estilo CSS -->
    <link rel="stylesheet" href="css/formulario.css">
    <link rel="stylesheet" href="css/info.css">
</head>
<body>
    <?php
    session_start(); 

    // Inicialización de variables de sesión para datos de usuario
    $_SESSION['nombre'] = isset($_SESSION['nombre']) ? $_SESSION['nombre'] : '';
    $_SESSION['primer_apellido'] = isset($_SESSION['primer_apellido']) ? $_SESSION['primer_apellido'] : '';
    $_SESSION['segundo_apellido'] = isset($_SESSION['segundo_apellido']) ? $_SESSION['segundo_apellido'] : '';

    
    if (!isset($_SESSION['num_cta'])) {
        header('Location: login.php'); 
        exit;
    }

    // Se maneja la información del formulario cuando se envía una solicitud HTTP de tipo POST
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Limpia y asigna valores del formulario a variables
        $nombre = isset($_POST['nombre']) ? htmlspecialchars($_POST['nombre']) : '';
        $primer_apellido = isset($_POST['primer_apellido']) ? htmlspecialchars($_POST['primer_apellido']) : '';
        $segundo_apellido = isset($_POST['segundo_apellido']) ? htmlspecialchars($_POST['segundo_apellido']) : '';
        $genero = isset($_POST['genero']) ? htmlspecialchars($_POST['genero']) : '';
        $fec_nac = isset($_POST['fec_nac']) ? htmlspecialchars($_POST['fec_nac']) : '';
        $contrasena = isset($_POST['contrasena']) ? htmlspecialchars($_POST['contrasena']) : '';

        // Agrega la información del alumno a la variable de sesión 'alumnos'
        $_SESSION['alumnos'][] = [
            'num_cta' => $_POST['num_cta'],
            'nombre' => $nombre,
            'primer_apellido' => $primer_apellido,
            'segundo_apellido' => $segundo_apellido,
            'genero' => $genero,
            'fec_nac' => $fec_nac,
            'contrasena' => $contrasena
        ];

        // Actualiza variables de sesión con datos del formulario
        $_SESSION['nombre'] = $nombre;
        $_SESSION['primer_apellido'] = $primer_apellido;
        $_SESSION['segundo_apellido'] = $segundo_apellido;
        $_SESSION['genero'] = $genero;
        $_SESSION['contrasena'] = $contrasena;
    }
    ?>

    <!-- Barra de opcines -->
    <div class="nav-container">
        <a href="#" class="active">Home</a>
        <a href="formulario.php">Registrar Alumnos</a>
        <a href="logout.php">Cerrar Sesión</a>
    </div>

    <!-- Contenedor principal -->
    <div class="container">
        <div class="login-container">
            <!-- Contenedor de información del usuario autenticado -->
            <div class="user-info-container">
                <h2>Usuario Autenticado</h2>
                <div class="user-container">
                    <div class="info-user">
                        <?php
                        // Muestra el nombre del usuario autenticado
                        if (isset($_SESSION['alumnos'])) {
                            $num_cta_buscado = $_SESSION['num_cta'];

                            foreach ($_SESSION['alumnos'] as $alumno) {
                                if ($alumno['num_cta'] === $num_cta_buscado) {
                                    echo '<p>' . $alumno['nombre'] . ' ' . $alumno['primer_apellido'] . ' ' . $alumno['segundo_apellido'] . '</p>';
                                    break;
                                }
                            }
                        }
                        ?>
                    </div>
                </div>

                <!-- Contenedor de información  -->
                <div class="info-container">
                    <h3>Información</h3>
                    <?php
                    // Muestra el número de cuenta y fecha de nacimiento del usuario autenticado
                    if (isset($_SESSION['alumnos'])) {
                        $num_cta_buscado = $_SESSION['num_cta'];

                        foreach ($_SESSION['alumnos'] as $alumno) {
                            if ($alumno['num_cta'] === $num_cta_buscado) {
                                echo '<p>Número de cuenta: ' . $alumno['num_cta'] . '</p>';
                                echo '<p>Fecha de nacimiento: ' . $alumno['fec_nac'] . '</p>';
                                break;
                            }
                        }
                    }
                    ?>
                </div>
            </div>

            <!-- Sección de datos guardados -->
            <h3>Datos Guardados</h3>
            <div class="info2-container">
                <?php
                // Muestra una tabla con los datos de todos los alumnos guardados
                if (isset($_SESSION['alumnos'])) {
                    echo '<table>';
                    echo '<tr><th>#</th><th>Nombre</th><th>Fecha de Nacimiento</th></tr>';

                    foreach ($_SESSION['alumnos'] as $alumno) {
                        echo '<tr>';
                        echo '<td>' . $alumno['num_cta'] . '</td>';
                        echo '<td>' . $alumno['nombre'] . ' ' . $alumno['primer_apellido'] . ' ' . $alumno['segundo_apellido'] . '</td>';
                        echo '<td>' . $alumno['fec_nac'] . '</td>';
                        echo '</tr>';
                    }

                    echo '</table>';
                }
                ?>
            </div>
        </div>
    </div>
</body>
</html>
