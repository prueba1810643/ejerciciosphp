<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <title>Argueta Bravo Angel Jacob</title>
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
    <?php
        session_start();

        // Se maneja la información del formulario cuando se envía una solicitud HTTP de tipo POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $numeroCuenta = $_POST['num_cta'];
            $contrasena = $_POST['contrasena'];

            // Verifica si las credenciales corresponden al administrador
            if ($numeroCuenta == '1' && $contrasena == 'adminpass123') {
                $_SESSION['num_cta'] = $numeroCuenta;
                $_SESSION['nombre'] = 'Admin';

                // Redirige al usuario autenticado al área protegida
                header('Location: info.php');
                exit;
            }

            // Verifica las credenciales de los alumnos almacenados en la sesión
            if (isset($_SESSION['alumnos'])) {
                foreach ($_SESSION['alumnos'] as $alumno) {
                    if ($numeroCuenta == $alumno['num_cta'] && $contrasena == $alumno['contrasena']) {
                        $_SESSION['num_cta'] = $alumno['num_cta'];
                        $_SESSION['nombre'] = $alumno['nombre'];

                        // Redirige al usuario autenticado al área protegida
                        header('Location: info.php');
                        exit;
                    }
                }
            }

            // Si las credenciales no coinciden, muestra un mensaje de error
            $error = "Cuenta o contraseña incorrectas. Por favor, intenta nuevamente.";
        }
    ?>
    <h2>Login</h2>
    <!-- Muestra el mensaje de error, si existe -->
    <?php if (isset($error)) : ?>
        <p style="color: red;"><?php echo $error; ?></p>
    <?php endif; ?>

    <!--  inicio de sesión -->
    <form method="post" action="">
        <label for="num_cta">Número de cuenta:</label>
        <input type="text" name="num_cta" required>
        <br>
        <label for="contrasena">Contraseña:</label>
        <input type="password" name="contrasena" required>
        <br>
        <input type="submit" value="Iniciar sesión">
    </form>
</body>
</html>
