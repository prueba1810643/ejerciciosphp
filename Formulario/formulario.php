<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <title>Argueta Bravo Angel Jacob</title>
    <link rel="stylesheet" href="css/formulario.css"> 
</head>
<body>
    <?php
        session_start(); // Inicia la sesión PHP

        // Verifica si la variable de sesión 'num_cta' está definida
        if (!isset($_SESSION['num_cta'])) {
            header('Location: login.php'); // Redirige a la página de inicio de sesión si no hay sesión activa
            exit;
        }

        // Se maneja la información del formulario cuando se envía una solicitud HTTP de tipo POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Agrega la información del alumno a la variable de sesión 'alumnos'
            $_SESSION['alumnos'][] = [
                'num_cta' => $_POST['num_cta'],
                'nombre' => $_POST['nombre'],
                'primer_apellido' => isset($_POST['primer_apellido']) ? $_POST['primer_apellido'] : '',
                'segundo_apellido' => isset($_POST['segundo_apellido']) ? $_POST['segundo_apellido'] : '',
                'genero' => isset($_POST['genero']) ? $_POST['genero'] : '',
                'fec_nac' => isset($_POST['fec_nac']) ? $_POST['fec_nac'] : '',
                'contrasena' => isset($_POST['contrasena']) ? $_POST['contrasena'] : ''
            ];
        }
    ?>

    <!-- Barra de opciones -->
    <div class="nav-container">
        <a href="info.php">Home</a>
        <a href="#" class="active">Registrar Alumnos</a>
        <a href="logout.php">Cerrar Sesión</a>
    </div>

    <!-- Contenedor del formulario de registro de alumnos -->
    <div class="login-container">
        <form method="post" action="">

            <!-- Campos del formulario -->
            <label for="num_cta">Número de cuenta:</label>
            <input type="text" name="num_cta" required placeholder="Número de cuenta">

            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre" required placeholder="Nombre">

            <label for="primer_apellido">Primer Apellido:</label>
            <input type="text" name="primer_apellido" required placeholder="Primer Apellido">

            <label for="segundo_apellido">Segundo Apellido:</label>
            <input type="text" name="segundo_apellido" required placeholder="Segundo Apellido">

            <label for="genero">Género:</label>
            <!-- Opciones de género  -->
            <div class="gender-container">
                <label><input type="radio" id="male" name="genero" value="M">Hombre</label>
                <label><input type="radio" id="female" name="genero" value="F">Mujer</label>
                <label><input type="radio" id="other" name="genero" value="O">Otro</label>
            </div>

            <label for="fec_nac">Fecha de Nacimiento:</label>
            <input type="date" name="fec_nac">

            <label for="contrasena">Contraseña:</label>
            <input type="password" name="contrasena" required placeholder="Contraseña">

            <!-- Botón de envío  -->
            <input type="submit" value="Registrar">
        </form>
    </div>
</body>
</html>
