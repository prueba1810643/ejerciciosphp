<?php
session_start(); 

unset($_SESSION['num_cta']); // Elimina la variable de sesión 'num_cta'
unset($_SESSION['nombre']); // Elimina la variable de sesión 'nombre'

header("Location: login.php"); // Redirige a la página de inicio de sesión
exit; // Finaliza la ejecución del script PHP
?>
